#include "List.hpp"
#include <iostream>

auto List::insert(int value) -> void {
    Node* ptr = new Node{value, nullptr};
    Node* p;
    if (first == nullptr)
        first = ptr;
    else {
        for (p = first; p->next != nullptr; p = p->next);
        p->next=ptr;

    }
}

auto  List::print() const -> void {
    for (Node* p = first; ; p = p->next) {
        if (p->next == nullptr) {
            std::cout <<"\033[1;37m"<< p->data;
            break;
        } else
            std::cout <<"\033[1;37m"<<p->data <<"\033[1;36m->";
    }
    std::cout << std::endl;

}

auto List::remove(int value) -> bool {
    if(first->data == value) {
        Node* p = first->next;
        delete first;
        first=p;
        return true;
    }
    Node* p = first;
    while(p->next != nullptr && p->next->data != value)
        p=p->next;
    if(p->next==nullptr) {
        return false;
    }
    Node* ptr = p->next;
    p->next=p->next->next;
    delete ptr;
    return true;
}

auto List::search(int value) const -> void {
    Node* p = first;
    int position=0;
    bool flag = false;
    while(p != nullptr) {
        if(p->data==value) {
            std::cout<<"\033[1;37m"<<position<<" ";
            flag = true;
        }
        p=p->next;
        position++;
    }
    if(!flag)
        std::cout<<"\033[1;31mЭлемент "<<value<<" не найден!\033[0;34m"<<std::endl;
    else
        std::cout<<std::endl;
}

auto List::change(int position,int value) -> bool {
    Node*p=first;
    for(std::size_t i = 0; i<position; i++) {
        if(p->next==nullptr) {
            return false;
        }
        p=p->next;
    }
    p->data = value;
    return true;
}

auto List::sort() -> void {
    for(Node* i = first; i; i=i->next) {
        for(Node* j = first; j; j=j->next) {
            if(j->data > i->data)
                std::swap(j->data,i->data);

        }
    }
}

List::~List() {
    Node* p = first;
    while (p->next != nullptr) {
        p = first->next;
        delete first;
        first = p;
    }
}

auto List::empty() const -> bool {
    if(first==nullptr)
        return false;
    return true;
}

void showmenu() {
    std::cout<<"\033[1;32mВыберите одну из операций:\033[0;34m"<<std::endl
             <<"1. Распечатать список"<<std::endl
             <<"2. Добавить элементы в список"<<std::endl
             <<"3. Удалить элемент"<<std::endl
             <<"4. Найти позиции элементов"<<std::endl
             <<"5. Заменить элемент на другой"<<std::endl
             <<"6. Отсортировать элементы списка"<<std::endl
             <<"7. Завершить работу программы"<<std::endl;
}

void error() {
    std::cin.clear();
    while (std::cin.get() != '\n');
    std::cout << "\033[1;31mВведено некорректное значение!" << std::endl;
}